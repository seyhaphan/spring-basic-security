package dev.springsecurity.domain;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;

class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    void findUserByUsername() {
        //given
      String username = "myadmin";

        //when
        UserCust user = userRepository.findUserByUsername(username).orElse(new UserCust());

        Assertions.assertThat(user).isNull();
    }
}