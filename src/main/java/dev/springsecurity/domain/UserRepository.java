package dev.springsecurity.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserCust,Long> {

    Optional<UserCust> findUserByUsername(String username);
}
