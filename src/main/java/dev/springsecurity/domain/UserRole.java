package dev.springsecurity.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum UserRole {
    ADMIN("ADMIN"),
    USER("USER");

    private final String value;

    private UserRole(String value) {
        this.value = value;
    }

    /**
     * Method getValue  : Get Enum value
     * @return Enum value
     */
    @JsonValue
    public String getValue() {
        return value;
    }
}
