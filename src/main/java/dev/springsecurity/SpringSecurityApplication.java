package dev.springsecurity;

import dev.springsecurity.domain.UserCust;
import dev.springsecurity.domain.UserRepository;
import dev.springsecurity.domain.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableJpaRepositories
public class SpringSecurityApplication {


    UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityApplication.class, args);
    }

    @PostConstruct
    public void init(){

        System.err.println(passwordEncoder.encode("user"));
//
//        UserCust user = new UserCust("myadmin",passwordEncoder.encode("myadmin"), UserRole.ADMIN);
//        userRepository.save(user);
    }
}
