package dev.springsecurity.controller;

import dev.springsecurity.domain.UserCust;
import dev.springsecurity.domain.UserRepository;
import dev.springsecurity.domain.UserRole;
import dev.springsecurity.exception.CUserNotFoundException;
import dev.springsecurity.payload.CreateUserRequest;
import dev.springsecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@Validated
public class MyRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/api/admin")
    public List<String> getTest(){

        return Arrays.asList("Apple","orange");
    }

    @GetMapping("/api/usertest")
    public List<String> getTest2(){

        return Arrays.asList("test2","test1");
    }

    @GetMapping("/api/user")
    public ResponseEntity<?> getUsers(){

        List<UserCust> users = userRepository.findAll();
        return ResponseEntity.ok(users);
    }

    @PostMapping("/api/user")
    public ResponseEntity<?> createUser(@Valid @RequestBody CreateUserRequest payload){

        userRepository.findUserByUsername(payload.getUsername()).ifPresent(userCust -> {
            throw new CUserNotFoundException("User exist");
        });

        UserCust user = UserCust.builder()
                .username(payload.getUsername())
                .password(passwordEncoder.encode(payload.getPassword()))
                .roles(UserRole.USER)
                .build();

        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
