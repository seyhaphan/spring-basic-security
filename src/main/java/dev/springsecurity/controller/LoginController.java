package dev.springsecurity.controller;

import dev.springsecurity.component.AuthenticationProvider;
import dev.springsecurity.config.jwt.JwtTokenUtil;
import dev.springsecurity.payload.LoginRequest;
import dev.springsecurity.payload.baseresponse.SingleResult;
import dev.springsecurity.service.ResponseService;
import dev.springsecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private UserService userService;

    @Autowired
    private ResponseService responseService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @PostMapping("/api/v1/login")
    public SingleResult login(@RequestBody LoginRequest payload) throws Exception{
        try {
            authenticationProvider.authenticate(payload.getUsername(),payload.getPassword());

             UserDetails userDetails = userService.loadUserByUsername(payload.getUsername());

            return responseService.getSingleResult(jwtTokenUtil.generateToken(userDetails));

        }catch (Exception e){
            throw new RuntimeException("Incorrect username or password");
        }

    }

}
