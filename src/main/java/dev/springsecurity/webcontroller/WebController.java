package dev.springsecurity.webcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {

    @GetMapping("/dashboard")
    public String index(){
        return "index";
    }

    @GetMapping("/admin")
    public String admin(){
        return "/admin";
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @GetMapping("/create")
    public String test(){
        return "/create_user_view";
    }
}
