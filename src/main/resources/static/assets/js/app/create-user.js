var user = {}

flex.extend({
    onload: () =>{
        user.getUsers();
    },
    events: () =>{

        $('#btn-create').on('click',()=>{
            user.createUser();
        })

    }
})

user.getUsers = () =>{
    comm.request.createAxiosRequest(comm.request.URL.USER,'GET',null,true, data =>{
        user.setDataToTable(data)
    })
}

user.setDataToTable = data =>{
    console.log(data)

    const row = data?.map( (user, index) => {
        const {id,username} = user
        return `<tr>
                    <td>${id}</td>
                    <td>${username}</td>
                </tr>`

    })

    $("#tb_user").html(row)
}


user.createUser = () =>{
    let userName = $("#username").val()
    let password = $("#password").val()

    let request = {
        username : userName,
        password // password: password
    }

    comm.request.createAxiosRequest(comm.request.URL.USER, 'POST', request, true, data =>{
        console.log(data)
        if(!flex.isNull(data)) return comm.ui.alertError(`Error `)

        comm.ui.alertSuccess(`User ${comm.static.successCreatedMsg}`)
    })
}