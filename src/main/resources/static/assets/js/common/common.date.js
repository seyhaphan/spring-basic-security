var comm;
if(!comm) comm={};
if(!comm.date)
{
    comm.date={};
    comm.date.formatDateTimeV1 = function(datetime, format)
    {
        return comm.date.commonFormatDateTime(datetime, format);
    }

    comm.date.formatDateTime = function(datetime)
    {
        return comm.date.commonFormatDateTime(datetime, "DD-MMM-YYYY HH:mm A");
        // return comm.date.commonFormatDateTime(datetime, "DD-MMM-YYYY HH:mm A");
    }

    comm.date.formatDate = function(datetime)
    {
        return comm.date.commonFormatDateTime(datetime, "DD-MM-YYYY");
    }

    comm.date.formatTime = function(datetime)
    {
        return comm.date.commonFormatDateTime(datetime, "HH:mm:ss");
    }

    comm.date.commonFormatDateTime = function(datetime, format)
    {
        return moment(datetime).tz('Asia/Phnom_Penh').format(format);
    }

}