var comm;
if(!comm) comm={};

if(!comm.ui) {
    comm.ui = {};

    comm.ui.modalOptions = {
        fadeDuration: 200,
        fadeDelay: 0.50,
        escapeClose: false,
        clickClose: false,
        showClose: false
    };

    comm.ui.alertMessage = function(msgType,title,message) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr[msgType](message, title)
    }
    comm.ui.alertSuccess = function(message){
        comm.ui.alertMessage("success", "Success", message);
    }
    comm.ui.alertWarning = function(message){
        comm.ui.alertMessage("warning", "Warning", message);
    }
    comm.ui.alertError = function(message){
        comm.ui.alertMessage("error", "Error", message);
    }
    comm.ui.alertInfo = function(message){
        comm.ui.alertMessage("info", "Information", message);
    }
    comm.ui.setMessage = function(selector, msg){
        //	if(flex.isNull(msg))
        //	{
        //		msg = "This field is required";
        //	}

        $(selector).show();
        $(selector).html(msg);
        $(selector).prev().addClass("my-error-input");
        $(selector).prev().focus();

    };

    // TODO: Remove hashtag (#...) when modal form pop up
    comm.ui.removeHashtagModalLink = function (elementId) {
        // 400 is the default speed for jQuery's hide().
        $(`${elementId}`).hide(400, function(){
            window.location.hash = '';
            history.pushState(null, null, window.location.href.split('#')[0]);
        });
    }

    // TODO: Remove validate error message label
    comm.ui.removeErrorInput = () => {
        $("input[type='text'], input[type='email'], input[type='password'],input[type='number'], #platform_option,select, textarea").on("keydown change",function(){
            $(this).next().hide();
            $("#email-invalid-error-message").hide()
            $(".text-block-27").show();
            $(this).removeClass("my-error-input");
            $(".search-button").show();
        });
    }

    comm.ui.removeErrorFromModal = () =>{

        $(document).on($.modal.OPEN, (event,modal)=>{
            let _this = $("input[type='text'], input[type='email'], textarea")
            _this.next().hide();
            _this.removeClass("my-error-input");
        });
    }

    comm.ui.checkURL = function(data, key, value){
        if(flex.isNull(data))
            return `?${key}=${value}`;
        else
            return `&${key}=${value}`;
    }
    comm.ui.generateNumbering = function(pgNo, pgSize, startNo) {
        return (startNo+1)+(Number(pgNo)*Number(pgSize))-Number(pgSize);
    };

    comm.ui.createProgressBar = () =>{

        NProgress.configure({
            ease: 'ease',
            speed: 500,
            trickleSpeed: 800,
            showSpinner: false
        });

        NProgress.start()
        NProgress.set(0.8);
        NProgress.inc();

    }

    comm.ui.destroyProgressBar = () =>{
        NProgress.done();
    }

    comm.ui.showLoadingProgress = function()
    {
        var origin;
        if (!window.location.origin)
        {
            origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
        else
        {
            origin = window.location.origin;
        }

        $("#gloading-container").remove();
        $("body").prepend("<div id='gloading-container'>" +
            "<div id='gloading-bacgnd'></div>" +
            "<div id='gloading-img'>" +
            "<h2>Loading...</h2>" +
            "<img src='/assets/js/jquery-plugins/loading/loading_apple.gif' />" +
            "</div>" +
            "</div>");
        $("body").css('cursor','wait');
    };

    comm.ui.hideLoadingProgress = function()
    {
        $("#gloading-bacgnd,#gloading-img").fadeOut(300);
        $("body").css('cursor','default');
    };

    comm.ui.pushStateV2 = function(page, perPage, title, startDate, endDate,category,answer, name, parent_id, area_id)
    {
        var result = '';

        var requestParam = new URLSearchParams(window.location.search);
        var paramPage    = requestParam.get('page_number');
        var paramPerPage = requestParam.get("page_size");
        var paramTitle   = requestParam.get("title");
        var paramStateDate   = requestParam.get("start_date");
        var paramEndDate   = requestParam.get("end_date");
        var paramCategory   = requestParam.get("category");
        var paramAnswer   = requestParam.get("answer");
        var paramParentId = requestParam.get("parent_id");
        var paramAreaId = requestParam.get("area_id");

        if(!flex.isNull(page))       paramPage    = page;
        if(!flex.isNull(perPage))    paramPerPage = perPage;
        if(!flex.isNull(title))      paramTitle   = title;
        if(!flex.isNull(startDate))      paramStateDate  = startDate;
        if(!flex.isNull(endDate))      paramEndDate   = endDate;
        if(!flex.isNull(category))      paramCategory   = category;
        if(!flex.isNull(answer))      paramAnswer   = answer;
        if(!flex.isNull(parent_id))      paramParentId  = parent_id;
        if(!flex.isNull(area_id))      paramAreaId   = area_id

        if(!flex.isNull(paramPage))    result += comm.ui.checkURL(result, 'page_number', paramPage);
        if(!flex.isNull(paramPerPage)) result += comm.ui.checkURL(result, 'page_size', paramPerPage);
        if(!flex.isNull(paramTitle))   result += comm.ui.checkURL(result, 'title'    , paramTitle);
        if(!flex.isNull(paramStateDate))   result += comm.ui.checkURL(result, 'start_date'    , paramStateDate);
        if(!flex.isNull(paramEndDate))   result += comm.ui.checkURL(result, 'end_date'    , paramEndDate);
        if(!flex.isNull(paramCategory))   result += comm.ui.checkURL(result, 'category'    , paramCategory);
        if(!flex.isNull(paramAnswer))   result += comm.ui.checkURL(result, 'answer' , paramAnswer);
        if(!flex.isNull(name))   result += comm.ui.checkURL(result, 'q' , name);
        if(!flex.isNull(paramAreaId))   result += comm.ui.checkURL(result, 'area_id' , paramAreaId);
        if(paramCategory == 'CAPITAL'){
            paramParentId=null;
        }
        if(paramCategory == 'BRAND'){
            paramParentId=null;
        }
        if(!flex.isNull(paramParentId))   result += comm.ui.checkURL(result, 'parent_id' , paramParentId);
        // console.log("helll",paramParentId)
        return result;
    }

    comm.ui.urlConcatV2 = function(page, perPage, title, startDate, endDate, category, answer, name)
    {
        var result = '';
        let currentPage = page === 0 ? 0 : page - 1 ;

        var requestParam = new URLSearchParams(window.location.search);
        var paramPage    = requestParam.get('page_number');
        var paramPerPage = requestParam.get("page_size");
        var paramTitle   = requestParam.get("title");
        var paramName   = requestParam.get("name");
        var paramStateDate   = requestParam.get("start_date");
        var paramEndDate   = requestParam.get("end_date");
        var paramCategory  = requestParam.get("category");
        var paramAnswer  = requestParam.get("answer");

        let cusParamPage = flex.isNull(paramPage) ? 0 : paramPage -1;

        if(!flex.isNull(currentPage))       cusParamPage    = currentPage;
        if(!flex.isNull(perPage))    paramPerPage = perPage;
        if(!flex.isNull(title))      paramTitle   = title;
        if(!flex.isNull(name))      paramName   = name;
        if(!flex.isNull(startDate))      paramStateDate   = startDate;
        if(!flex.isNull(endDate))      paramEndDate   = endDate;
        if(!flex.isNull(category))      paramCategory   = category;
        if(!flex.isNull(answer))      paramAnswer   = answer;

        if(!flex.isNull(cusParamPage))    result += comm.ui.checkURL(result, 'page_number', cusParamPage);
        if(!flex.isNull(paramPerPage)) result += comm.ui.checkURL(result, 'page_size', paramPerPage);
        if(!flex.isNull(paramTitle))   result += comm.ui.checkURL(result, 'title'    , paramTitle);
        if(!flex.isNull(paramName))   result += comm.ui.checkURL(result, 'name'    , paramName);
        if(!flex.isNull(paramStateDate))   result += comm.ui.checkURL(result, 'start_date'    , paramStateDate);
        if(!flex.isNull(paramEndDate))   result += comm.ui.checkURL(result, 'end_date'    , paramEndDate);
        if(!flex.isNull(paramCategory))   result += comm.ui.checkURL(result, 'category' , paramCategory);
        if(!flex.isNull(paramAnswer))   result += comm.ui.checkURL(result, 'answer' , paramAnswer);

        return result;
    }

    comm.ui.display = function(option)
    {
        if(flex.isNull(option)) option = false;
        if(option)
        {
            var t = $(".dropdown-toggle-quick-create").attr("aria-expanded");
            if(flex.isNull(t)) t = "false";
            if(t == "true")
            {
                $("#btn-quick-create").attr("style", "");
                $(".dropdown-toggle-quick-create").attr("aria-expanded", false);
                $(".dropdown-toggle-quick-create").removeClass("w--open");
                $(".dropdown-list-quick-create").removeClass("w--open");

            }
            else
            {
                $("#btn-quick-create").attr("style", "z-index:901;");
                $(".dropdown-toggle-quick-create").attr("aria-expanded", true);
                $(".dropdown-toggle-quick-create").addClass("w--open");
                $(".dropdown-list-quick-create").addClass("w--open");
                $(".jquery-modal").attr("style", "z-index:900!important;");
            }
        }
        else
        {
            $("#btn-quick-create").attr("style", "");
            $(".dropdown-toggle-quick-create").attr("aria-expanded", false);
            $(".dropdown-toggle-quick-create").removeClass("w--open");
            $(".dropdown-list-quick-create").removeClass("w--open");
        }
    };

    comm.ui.showFooter = function(tbody)
    {
        const rowCount = $(tbody).length;
        if(rowCount === 0)
            $("#div-footer").show();
    }
    comm.ui.getApiUrl = function(keySearch, url, pageSize, pageNumber, sortBy)
    {
        var requestParam = new URLSearchParams(window.location.search);
        var title = requestParam.get(keySearch);
        var apiUrl = `${url}?${keySearch}=${title}&page_number=${pageNumber}&page_size=${pageSize}&sort_by=${sortBy}`;
        if(flex.isNull(title)) apiUrl = `${url}?page_number=${pageNumber}&page_size=${pageSize}&sort_by=${sortBy}`;

        return apiUrl;
    }

    //Other Case for name or for "area_id"
    comm.ui.urlConcatV1ForNameOrAreaId = function(page, perPage, name, areaId)
    {
        var result = '';
        let currentPage = page === 0 ? 0 : page - 1 ;

        var requestParam = new URLSearchParams(window.location.search);
        var paramPage    = requestParam.get('page_number');
        var paramPerPage = requestParam.get("page_size");
        var paramName   = requestParam.get("name");
        var paramAreaId   = requestParam.get("area_id");

        let cusParamPage = flex.isNull(paramPage) ? 0 : paramPage -1;

        if(!flex.isNull(currentPage))       cusParamPage    = currentPage;
        if(!flex.isNull(perPage))    paramPerPage = perPage;
        if(!flex.isNull(name))      paramName   = name;
        if(!flex.isNull(paramAreaId))      paramAreaId   = areaId;

        if(!flex.isNull(cusParamPage)) {
            result += comm.ui.checkURL(result, 'page_number', cusParamPage);
        }
        if(!flex.isNull(paramPerPage)) result += comm.ui.checkURL(result, 'page_size', paramPerPage);
        if(!flex.isNull(paramName))   result += comm.ui.checkURL(result, 'name'    , paramName);
        if(!flex.isNull(paramAreaId))   result += comm.ui.checkURL(result, 'area_id'    , paramAreaId);

        return result;
    }

    // TODO: Validation on input
    comm.ui.validationInputNumeric = function(elementId) {
        $(`${elementId}`).mask('ZZZZZZZZZZZZZZZZZ',{translation: {
                'Z': {
                    pattern: /^[0-9.]/, optional: true
                }
            }
        });
    }

}