var comm;
if(!comm) comm={};
if(!comm.request) {
    comm.request = {};

    comm.request.URL = {
        USER: '/api/user',

    }

    comm.request.createAxiosRequest = function(url, method, input,isLoading,callBackFn){

        if(isLoading == undefined) isLoading = true;
        if(isLoading) comm.ui.createProgressBar();

        if(!input) var input = {};

        axios({
            method: method,
            url: url,
            data: input
        })
        .then(({data}) =>{
            callBackFn(data)
            comm.ui.destroyProgressBar()
        })
        .catch((err) =>{
            callBackFn(err?.response?.data)
            comm.ui.destroyProgressBar()
        })

    }

    comm.request.createAjaxRequest = function(url, method, token, input, callbackFn, isLoading)
    {
        if(token == null || token == undefined) token = comm.request.readCookie("MY_TOKEN_DEV")
        if(isLoading == undefined) isLoading = true;
        if(isLoading) comm.ui.showLoadingProgress();

        input = JSON.stringify(input);

        if(!input) var input = {};

        if(!flex.isNull(token))
        {
            $.ajaxSetup({
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
        }

        $.ajax({
            async      : false,
            type       : method,
            data       : input,
            //dataType   : 'json',
            contentType: "application/json; charset=utf-8",
            url        : url,
            cache      : false,
            success    : function(dat)
            {
                if(!flex.isNull(dat.url))
                {
                    alert("Session is timed out.");
                    window.location.replace(dat.url);
                }
                callbackFn(dat);
                if(isLoading) comm.ui.hideLoadingProgress();
            },
            error       : function (err)
            {
                if(isLoading) comm.ui.hideLoadingProgress();
                //alert(JSON.stringify(err, null, 2));
                var errObj = {};
                errObj["status"] = err.status;
                // errObj["error"]  = err.responseJSON.error;
                callbackFn(errObj);
            },
        });
    }

}