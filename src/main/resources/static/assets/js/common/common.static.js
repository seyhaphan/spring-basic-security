var comm;
if(!comm) comm={};
if(!comm.static)
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
today.setMonth(today.getMonth()-1)
var dd1=String(today.getDate()).padStart(2, '0');
var mm1 = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy1 = today.getFullYear();
var firstDay = new Date(yyyy, mm, 1);
var lastDay = new Date(yyyy, mm + 1, 0);
{
    comm.static = {};
    comm.static.pageNumber = 0;
    comm.static.pageSize = 10;
    comm.static.visiblePages = 5;
    comm.static.sortBy = "desc";
    comm.static.successCreatedMsg = " has been created successfully.";
    comm.static.successUpdatedMsg = " has been updated successfully.";
    comm.static.successDeletedMsg = " has been deleted successfully.";
    comm.static.commonFormatStartDate=yyyy1 + '-' + mm1 + '-' + dd1;
    comm.static.commonFormatEndDate=yyyy + '-' + mm + '-' + dd;
}